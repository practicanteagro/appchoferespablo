package com.example.myapplication

import com.example.myapplication.Model.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

//Hay que hacerle ajustes.
interface APIService {
    @GET("login")
    fun getUser(
        @Path("username")username: String,
        @Path("password")password: String): Call<List<User>>
}