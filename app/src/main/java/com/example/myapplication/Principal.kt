package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_principal.*
import kotlin.random.Random

class Principal : AppCompatActivity() {
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var recyclerView: RecyclerView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView?.layoutManager = linearLayoutManager
        recyclerView = findViewById(R.id.recyclerView)
        var adapter = UserAdapter(generateData())
        recyclerView?.layoutManager = LinearLayoutManager(applicationContext)
        recyclerView?.itemAnimator = DefaultItemAnimator()
        recyclerView?.adapter = adapter
        adapter.notifyDataSetChanged()
        val objeto:Intent = intent
        var nombre = objeto.getStringExtra("Nombre")
        //textView.setText("Usuario: $nombre")
        val lista = arrayOf("Pendiente", "Realizadas", "Canceladas")
        val adapatador1 = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista)
        spinner.adapter = adapatador1
        val lista2 = arrayOf("Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
        val adapatador2 = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista2)
        spinner2.adapter = adapatador2
        button3.setOnClickListener{
            val intento1 = Intent(this, Login::class.java)
            startActivity(intento1)
            finish()
        }
    }
    private fun generateData(): ArrayList<Datos> {
        var result = ArrayList<Datos>()
        for(i in 0..9){
            var user:Datos = Datos("Lugar: ", "Fecha:")
            result.add(user)
        }
        return result
    }
}