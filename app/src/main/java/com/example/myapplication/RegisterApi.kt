package com.example.myapplication

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path

public interface RegisterApi {
    @Headers("Autorization: application/x-www-form-urlencoded")
    @POST("login")
    fun login(@Path("username") username:String, @Path("password") password:String):Call<ResponseBody>
}