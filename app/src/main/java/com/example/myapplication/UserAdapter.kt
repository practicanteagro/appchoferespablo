package com.example.myapplication

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView
import androidx.appcompat.view.menu.MenuView.ItemView
import androidx.recyclerview.widget.RecyclerView

class UserAdapter(private var items: ArrayList<Datos>): RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    override fun getItemCount(): Int {
        return items.size
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var Datos= items[position]
        holder?.txtName?.text=Datos.Lugar
        holder?.txtComment?.text=Datos.Fecha_Salida
        //Eventos de Botones
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView=LayoutInflater.from(parent?.context).inflate(R.layout.userlist, parent, false)
        return ViewHolder(itemView)
    }
    class ViewHolder(row:View): RecyclerView.ViewHolder(row){
        var txtName:TextView? = null
        var txtComment:TextView? = null
        init {
            this.txtName = row?.findViewById<TextView>(R.id.txtLugar)
            this.txtComment = row?.findViewById<TextView>(R.id.txtFecha)

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, Detalles::class.java)
                itemView.context.startActivity(intent)
            }
        }
    }
}