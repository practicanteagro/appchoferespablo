package com.example.myapplication.Model

import com.google.gson.annotations.SerializedName

class User{
    @SerializedName("username")
    var username = ""
    @SerializedName("password")
    var password = ""
}