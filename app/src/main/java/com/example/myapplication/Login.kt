
package com.example.myapplication

import android.content.Context
import android.content.Intent
import  androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.myapplication.Model.User
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_principal.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val checkBox = findViewById(R.id.checkBox) as CheckBox
        val preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE)
        editText.setText(preferencias.getString("mail", ""))
        editText2.setText(preferencias.getString("contraseña", ""))
        checkBox.isChecked = loadCheckBox(checkBox.text.toString())

        button.setOnClickListener{
            var nom = editText.text.toString()
            var con = editText2.text.toString()
            val editor= preferencias.edit()
            var retrofit:Retrofit = Retrofit.Builder()
                .baseUrl("http://172.24.4.41:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            var service = retrofit.create(APIService::class.java)
            val user = service.getUser(editText.toString(), editText2.toString())
            user.enqueue(object : Callback<List<User>>{
                override fun onFailure(call: Call<List<User>>, t: Throwable) {
                    Log.v("retrofit", "call failed")
                    Log.e("ERROR", t.toString())
                }
                override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                    Log.i("RESULTADO", response.toString())
                    }
                })
            if(nom=="Pablo"){
                if(con=="1234"){
                    if(checkBox.isChecked){
                        editor.putString("mail", editText.text.toString())
                        editor.putString("contraseña", editText2.text.toString())
                        editor.putBoolean("CheckBox_Value", true)
                        editor.commit()
                    }
                    else{
                        editText.setText("")
                        editText2.setText("")
                        editor.putString("mail", editText.text.toString())
                        editor.putString("contraseña", editText2.text.toString())
                        editor.commit()
                    }
                    Toast.makeText(this, "Bienvenido!!", Toast.LENGTH_LONG).show()
                    val intento1 = Intent(this, Principal::class.java)
                    intento1.putExtra("Nombre", nom)
                    startActivity(intento1)
                    finish()
                }
                else{
                    Toast.makeText(this, "La contraseña es incorrecta", Toast.LENGTH_LONG).show()
                }
            }
            else{
                if(nom=="Erick"){
                    if(con=="4321"){
                        if(checkBox.isChecked){
                            editor.putString("mail", editText.text.toString())
                            editor.putString("contraseña", editText2.text.toString())
                            editor.putBoolean("CheckBox_Value", true)
                            editor.commit()
                        }
                        else{
                            editText.setText("")
                            editText2.setText("")
                            editor.putString("mail", editText.text.toString())
                            editor.putString("contraseña", editText2.text.toString())
                            editor.putBoolean("CheckBox_Value", false)
                            editor.commit()
                        }
                        Toast.makeText(this, "Bienvenido!!", Toast.LENGTH_LONG).show()
                        val intento1 = Intent(this, Principal::class.java)
                        intento1.putExtra("Nombre", nom)
                        startActivity(intento1)
                        finish()
                    }
                    else{
                        Toast.makeText(this, "La contraseña es incorrecta", Toast.LENGTH_LONG).show()
                    }
                }
                else{
                    if(nom=="Julio"){
                        if(con=="0000"){
                            if(checkBox.isChecked){
                                editor.putString("mail", editText.text.toString())
                                editor.putString("contraseña", editText2.text.toString())
                                editor.putBoolean("CheckBox_Value", true)
                                editor.commit()
                            }
                            else{
                                editText.setText("")
                                editText2.setText("")
                                editor.putString("mail", editText.text.toString())
                                editor.putString("contraseña", editText2.text.toString())
                                editor.putBoolean("CheckBox_Value", false)
                                editor.commit()
                            }
                            Toast.makeText(this, "Bienvenido!!", Toast.LENGTH_LONG).show()
                            val intento1 = Intent(this, Principal::class.java)
                            intento1.putExtra("Nombre", nom)
                            startActivity(intento1)
                            finish()
                        }
                        else{
                            Toast.makeText(this, "La contraseña es incorrecta", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }
    }
    private fun loadCheckBox(key: String): Boolean {
        val sharedPreferences = getPreferences(Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(key, false)
    }
}
